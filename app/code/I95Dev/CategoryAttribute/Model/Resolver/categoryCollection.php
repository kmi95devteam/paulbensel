<?php

namespace I95Dev\CategoryAttribute\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;


class categoryCollection implements ResolverInterface
{

    /**
     *
     * @var blockRepository
     */
    private $blockRepository;


    /**
     *
     * @var searchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     *
     * @var categoryRepository
     */
    private $categoryRepository;



    /**
     *
     * @var Resource
     */
    protected $resource;

    /**
     *
     * @var StoreManager
     */
    protected $storeManager;



    /**
     * @param SliderModelFactory $sliderModelFactory
     * @param BannerModel $bannerModel
     * @param Resource $resource
     * @param StoreManager $storeManager
     */
    public function __construct(
        BlockRepositoryInterface  $blockRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\App\ResourceConnection $Resource,
        StoreManagerInterface $storeManager
    ) {
        $this->blockRepository = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;
        $this->resource = $Resource;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $categoryID = $args['id'];
        $categoryData=$this->getCategoryData($categoryID);
        return $categoryData;
    }


    /**
     * @param  array $args
     * @return array
     * @throws GraphQlNoSuchEntityException
     */

    private function getCategoryData(int $categoryId):array
    {

        $collection = $this->categoryRepository->get($categoryId);
        $cmsBlockID = $collection->getData('cms_block_list');

        $searchCriteria = $this->searchCriteriaBuilder->addFilter('block_id', $cmsBlockID, 'eq')->create();
        $cmsBlock = $this->blockRepository->getList($searchCriteria)->getItems();

        $blockData=[];
        if(count($cmsBlock) > 0){
            foreach($cmsBlock as  $cmsCollection) {
                $blockData[] = [
                    'id' => $collection->getData('entity_id'),
                    'cms_block_list' => $collection->getData('cms_block_list'),
                    'block_id' => $cmsCollection->getData('block_id'),
                    'identifier' => $cmsCollection->getData('identifier'),
                    'content' => $cmsCollection->getData('content'),
                ];
            }
        }

        return $blockData;

    }



}