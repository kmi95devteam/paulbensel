<?php
namespace I95Dev\Storebanners\Controller\Adminhtml\i95devstorebanners;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class Save extends \Magento\Backend\App\Action
{
        /**
        * @var \Magento\Framework\Image\AdapterFactory
        */
        protected $adapterFactory;
        /**
        * @var \Magento\MediaStorage\Model\File\UploaderFactory
        */
        protected $uploader;
        /**
        * @var \Magento\Framework\Filesystem
        */
        protected $filesystem;
        /**
        * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
        */
        protected $timezoneInterface;
        /**
         * @var \Magento\Framework\Module\Dir\Reader
         */
        protected $moduleReader;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context,\Magento\Framework\Module\Dir\Reader $moduleReader,\Magento\Framework\Image\AdapterFactory $adapterFactory,\Magento\MediaStorage\Model\File\UploaderFactory $uploader,\Magento\Framework\Filesystem $filesystem)
    {
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        $this->moduleReader = $moduleReader;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        //echo "<pre>";print_r($data);die;       
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $model = $this->_objectManager->create('I95Dev\Storebanners\Model\i95devstorebanners');

            $id = $this->getRequest()->getParam('id');
        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
        $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $media  = $mediaPath.'wysiwyg/banner/';
        
        $file_name = rand() . $_FILES['image_path']['name'];
        $file_size = $_FILES['image_path']['size'];
        $file_tmp = $_FILES['image_path']['tmp_name'];
        $file_type = $_FILES['image_path']['type'];
        $viewDir = $this->moduleReader->getModuleDir(
            \Magento\Framework\Module\Dir::MODULE_VIEW_DIR,
            'I95Dev_Storebanners'
        );
        $imgname = $media . $file_name;
         if (move_uploaded_file($file_tmp, $imgname)) {
            echo $file_name;
        } else {
            echo "File was not uploaded";
        }
 
        //$media = $viewDir . '/frontend/web/images/';
       
      $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface'); 
//$currentStore = $storeManager->getStore();
      $currentStore = $storeManager->getStore()->getBaseUrl();

 //$mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

$foldr_path  = $currentStore.'media/wysiwyg/banner/';
//       echo '<pre>'; print_r($_FILES['image_path']); 
        //$foldr_path = $viewDir . '/frontend/web/images/';
        $data['image_path'] = $foldr_path . $file_name;
        //echo $data['image_path'];die;
    //$data['image_path'] = $imgname;
     
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('I95Dev\Storebanners\Model\i95devstorebanners');

            $id = $this->getRequest()->getParam('id');
           //echo "<pre>";print_r($model->load($id)->toArray()).PHP_EOL;echo count($model->load($id)->toArray());die;
            if (count($model->load($id)->toArray())!=0) {
                //$previmg = $model->load($id)->toArray();
               //unlink($previmg["image_path"]);
                //$model->load($id);
                //$model->setCreatedAt(date('Y-m-d H:i:s'));
                if($_FILES['image_path']['name'] != ''){
                $model->setData("image_path",$data["image_path"]);
                }

                $model->setData("banner_title",$data["banner_title"]);
                $model->setData("banner_content",$data["banner_content"]);
                $model->setData("banner_specialcontent",$data["banner_specialcontent"]);
                $model->setData("banner_link",$data["banner_link"]);
                $model->save();
            } else {
			
                // echo "<pre>";print_r($data);die;
            $model->setData($data);

            try {
                 
                $model->save();
                $this->messageManager->addSuccess(__('The Banner has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Banner.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        } }
        return $resultRedirect->setPath('*/*/');
     } 
}