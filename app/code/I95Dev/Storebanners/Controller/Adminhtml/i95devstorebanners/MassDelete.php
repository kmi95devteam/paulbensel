<?php
namespace I95Dev\Storebanners\Controller\Adminhtml\i95devstorebanners;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $itemIds = $this->getRequest()->getParam('i95devstorebanners');
        if (!is_array($itemIds) || empty($itemIds)) {
            $this->messageManager->addError(__('Please select Flyers(s).'));
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $post = $this->_objectManager->get('I95Dev\Storebanners\Model\i95devstorebanners')->load($itemId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($itemIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('storebanners/*/index');
    }
}