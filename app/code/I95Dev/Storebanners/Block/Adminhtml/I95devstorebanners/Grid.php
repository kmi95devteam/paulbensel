<?php
namespace I95Dev\Storebanners\Block\Adminhtml\I95devstorebanners;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \I95Dev\Storebanners\Model\i95devstorebannersFactory
     */
    protected $_i95devimagedealsFactory;

    /**
     * @var \I95Dev\Storebanners\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \I95Dev\Storebanners\Model\i95devstorebannersFactory $i95devimagedealsFactory
     * @param \I95Dev\Storebanners\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \I95Dev\Storebanners\Model\i95devstorebannersFactory $I95devimagedealsFactory,
        \I95Dev\Storebanners\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_i95devimagedealsFactory = $I95devimagedealsFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_i95devimagedealsFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'image_path',
					[
						'header' => __('Image Path'),
						'index' => 'image_path',
					]
				);
				
				
                                $this->addColumn(
					'banner_title',
					[
						'header' => __('Banner Title'),
						'index' => 'banner_title',
					]
				);
                                $this->addColumn(
					'banner_content',
					[
						'header' => __('Banner Content'),
						'index' => 'banner_content',
					]
				);
                                $this->addColumn(
					'banner_specialcontent',
					[
						'header' => __('Banner Specialcontent'),
						'index' => 'banner_specialcontent',
					]
				);
                                  $this->addColumn(
					'banner_link',
					[
						'header' => __('Banner Link'),
						'index' => 'banner_link',
					]
				);
				


		
        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );
		

		
		   $this->addExportType($this->getUrl('storebanners/*/exportCsv', ['_current' => true]),__('CSV'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('I95Dev_Storebanners::i95devstorebanners/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('i95devstorebanners');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('storebanners/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('storebanners/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('storebanners/*/index', ['_current' => true]);
    }

    /**
     * @param \I95Dev\Storebanners\Model\i95devstorebanners|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'storebanners/*/edit',
            ['id' => $row->getId()]
        );
		
    }

	

}