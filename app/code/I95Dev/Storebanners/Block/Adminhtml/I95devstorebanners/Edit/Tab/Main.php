<?php

namespace I95Dev\Storebanners\Block\Adminhtml\I95devstorebanners\Edit\Tab;

/**
 * i95devstorebanners edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    

    
    /**
     * @var \I95Dev\Storebanners\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \I95Dev\Storebanners\Model\Status $status,
        array $data = []
    ) {
         
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

        /**
     * @return $this
     */
   protected function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $locationData =  $objectManager->create("\I95DevConnect\Warehouse\Model\WarehouseData")->getCollection();
        $Storecollection = [];

        foreach ($locationData as $location) {
            $Storecollection[] =  [ 'value' => $location->getCity(), 'label' => $location->getCity() ];
        }
        return $Storecollection;
    }
    
    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \I95Dev\Storebanners\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('i95devstorebanners');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Banner Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

		
        $fieldset->addField(
            'image_path',
                'image',
            [
                'name' => 'image_path',
                'label' => __('Image'),
                'title' => __('Image'),
		'note' => 'Allow image type: jpg, jpeg, gif, png',		
                'disabled' => $isElementDisabled
            ]
        );
					

		
		 $fieldset->addField(
            'banner_title',
            'text',
            [
                'name' => 'banner_title',
                'label' => __('Banner Title'),
                'title' => __('Banner Title'),
                'maxlength' => '45'
            ]
        );
		
		 $fieldset->addField(
            'banner_content',
            'text',
            [
                'name' => 'banner_content',
                'label' => __('Banner Content'),
                'title' => __('Banner Content'),
                'maxlength' => '200'
            ]
        );
         
       	 $fieldset->addField(
            'banner_specialcontent',
            'text',
            [
                'name' => 'banner_specialcontent',
                'label' => __('Banner Specialcontent'),
                'title' => __('Banner Specialcontent'),
                'maxlength' => '20'
            ]
        ); 
        $fieldset->addField(
            'banner_link',
            'text',
            [
                'name' => 'banner_link',
                'label' => __('Banner Link'),
                'title' => __('Banner Link'),
                'maxlength' => '200'
            ]
        );         
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Banner Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Banner Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
