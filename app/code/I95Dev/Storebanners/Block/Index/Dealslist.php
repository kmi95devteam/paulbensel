<?php

namespace I95Dev\Storebanners\Block\Index;


class Dealslist extends \Magento\Framework\View\Element\Template {

    protected $i95devimagedealsModelFactory;
    protected $_storeManager;    
    
    public function __construct(
            \Magento\Catalog\Block\Product\Context $context, 
            \I95Dev\Storebanners\Model\i95devstorebannersFactory $i95devimagedealsModelFactory,
            \Magento\Store\Model\StoreManagerInterface $storeManager,        

            
            array $data = []
            ) {

        parent::__construct($context, $data);
         $this->_i95devimagedealsModelFactory = $i95devimagedealsModelFactory;
         $this->_storeManager = $storeManager;        


    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
 /**
     * Get store identifier
     *
     * @return  int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * Get website identifier
     *
     * @return string|int|null
     */
    public function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    /**
     * Get Store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }

    public function getImagedeals()
    {
        $collection = $this->_i95devimagedealsModelFactory->create()->getCollection();
        return $collection;
    }

    public function getVideodeals()
    {
        $collection = $this->_i95devvideodealsModelFactory->create()->getCollection();
        return $collection;
    }

}