<?php
namespace I95Dev\Storebanners\Block\Index;

use Magento\Framework\App\RequestInterface;
use I95Dev\Storebanners\Model\i95devstorebanners;

class Storebanner extends \Magento\Framework\View\Element\Template 
{
    protected $i95devstorebanners;
    protected $request;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        i95devstorebanners $i95devstorebanners,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_i95devstorebanners = $i95devstorebanners;
        $this->_request = $request;
    }

    public function getBannercollection()
    {
        $collection = $this->_i95devstorebanners->getCollection();
        return $collection;
    }
}