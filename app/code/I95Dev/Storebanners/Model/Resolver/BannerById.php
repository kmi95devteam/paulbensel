<?php

namespace I95Dev\Storebanners\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use I95Dev\Storebanners\Model\i95devstorebannersFactory as i95devstorebannersFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;


class BannerById implements ResolverInterface
{
    /**
     *
     * @var i95devstorebannersFactory
     */
    private $i95devstorebannersFactory;



    /**
     *
     * @var Resource
     */
    protected $resource;

    /**
     *
     * @var StoreManager
     */
    protected $storeManager;



    /**
     * @param SliderModelFactory $sliderModelFactory
     * @param BannerModel $bannerModel
     * @param Resource $resource
     * @param StoreManager $storeManager
     */
    public function __construct(
        i95devstorebannersFactory $i95devstorebannersFactory,
        \Magento\Framework\App\ResourceConnection $Resource,
        StoreManagerInterface $storeManager
    ) {
        $this->i95devstorebannersFactory = $i95devstorebannersFactory;
        $this->resource = $Resource;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $bannersData=$this->getbannersData($args);
        return $bannersData;
    }


    /**
     * @param  array $args
     * @return array
     * @throws GraphQlNoSuchEntityException
     */

    private function getbannersData(array $args):array
    {
        $data=[];
        $collection=$this->i95devstorebannersFactory->create()->getCollection();
       
        $bannersData=[];
        if(count($collection) > 0){
            foreach($collection as  $banners) {
                $bannersData[] = [
                    'id' => $banners->getData('id'),
                    'image_path' => $banners->getData('image_path'),
                    'banner_title' => $banners->getData('banner_title'),
                    'banner_content' => $banners->getData('banner_content'),
                    'banner_specialcontent' => $banners->getData('banner_specialcontent'),
                    'banner_link' => $banners->getData('banner_link'),

                ];
            }
        }

        return $bannersData;

    }
    


}