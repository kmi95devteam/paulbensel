<?php
namespace I95Dev\Storebanners\Model;

class i95devstorebanners extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('I95Dev\Storebanners\Model\ResourceModel\i95devstorebanners');
    }
}
?>