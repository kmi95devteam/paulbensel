<?php

namespace I95Dev\Storebanners\Model\ResourceModel\i95devstorebanners;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('I95Dev\Storebanners\Model\i95devstorebanners', 'I95Dev\Storebanners\Model\ResourceModel\i95devstorebanners');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>