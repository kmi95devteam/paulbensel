<?php
namespace I95Dev\Storebanners\Model\ResourceModel;

class i95devstorebanners extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('i95dev_storebanners', 'id');
    }
}
?>