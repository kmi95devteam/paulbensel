import React, { useEffect } from 'react';
import { useQuery } from '@apollo/client';
import bannerOperations from './banner.gql';
import mergeClasses from '@magento/venia-ui/lib/classify';
import SlickSlider from 'react-slick';
import { Link } from 'react-router-dom';
import PageLoadingIndicator from '@magento/venia-ui/lib/components/PageLoadingIndicator';
import sliderClasses from "@magento/pagebuilder/lib/ContentTypes/Slider/slider.css";
import defaultClasses from './banner.css';


const banner = props => {
    var sliderSettings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay:false,
        autoplaySpeed: 5000
    };
   

    const { queries } = bannerOperations;
    const { getBannerQuery } = queries;

    // Fetch the data using apollo react hooks

    const { data, error, loading } = useQuery(getBannerQuery, {
        variables: { id: props.id }
    });

    if (loading) {
        return '';
    }

    if (error) {
        return <p>Error ....</p>;
    }

    var sliderSettings = data.bannerId ? {
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnHover: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    } : null;
    const {bann}=data;
    const bannerListItems = data.bannerId.map((bannerListItem) => {

        return (bannerListItem.banner_link.length > 0) ? (

            <div className={defaultClasses.slides} key={bannerListItem.id}>
            <img className={defaultClasses.custimage}  src={bannerListItem.image_path} />
            <div className={defaultClasses.contentblock}>
        <div className={defaultClasses.bantitle}><span>{bannerListItem.banner_title}</span></div>
        <div className={defaultClasses.bancontent}><span>{bannerListItem.banner_content}</span></div>
        <a className={defaultClasses.banlink} href={bannerListItem.banner_link}>Shop Now</a>
        </div>
        </div>
    ):(<div className={defaultClasses.slides} key={bannerListItem.id}>
            <img className={defaultClasses.custimage}  src={bannerListItem.image_path} />
            <div className={defaultClasses.contentblock}>
        <div className={defaultClasses.bantitle}><span>{bannerListItem.banner_title}</span></div>
        <div className={defaultClasses.bancontent}><span>{bannerListItem.banner_content}</span></div>
            </div>
        </div>
    );
    });
    return <div className={sliderClasses.root}>
        <SlickSlider {...sliderSettings}>{bannerListItems}</SlickSlider></div>;

};

export default banner;