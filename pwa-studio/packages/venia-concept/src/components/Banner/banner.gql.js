import gql from 'graphql-tag'

const GET_BANNER_QUERY = gql`
query banners($id: Int )
{
	bannerId(id: $id)
  {
    id
    image_path
    banner_title
    banner_content
    banner_specialcontent
    banner_link
    
  }
}
`;

export default {
    queries: {
        getBannerQuery: GET_BANNER_QUERY
    },
    mutations: {}
};