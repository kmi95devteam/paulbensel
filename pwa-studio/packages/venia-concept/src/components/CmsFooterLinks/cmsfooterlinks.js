import React, { Fragment, Suspense, useState  } from 'react';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import defaultClasses from './cmsfooterlinks.css';
import cmsBlock from "../CmsBlock";

const links = props => {
    const getInTouch = cmsBlock({"identifiers": "getintouch"});
    const aboutUs = cmsBlock({"identifiers": "aboutus"});
    const Services = cmsBlock({"identifiers": "services"});
    const customerCare = cmsBlock({"identifiers": "customercare"});
    const [toggle1, setToggle1] = useState(false);
    const [toggle2, setToggle2] = useState(false);
    const [toggle3, setToggle3] = useState(false);
    const [toggle4, setToggle4] = useState(false);

    return(
        <Fragment>
        <div className={defaultClasses.mobilefooterlinkssection}>
            <div className={defaultClasses.footersubsection}>
                <div className={defaultClasses.footerblock}>
                    <div onClick={() => setToggle1(!toggle1)} className={toggle1 ? defaultClasses.active : defaultClasses.inactive }><h3>Get in touch</h3></div>
                        {toggle1 && <div>{getInTouch}</div>}
                </div>
                <div className={defaultClasses.footerblock}>
                    <div onClick={() => setToggle2(!toggle2)} className={toggle2 ? defaultClasses.active : defaultClasses.inactive }><h3>About us</h3></div>
                        {toggle2 && <div>{aboutUs}</div>}
                </div>
                <div className={defaultClasses.footerblock}>
                    <div onClick={() => setToggle3(!toggle3)} className={toggle3 ? defaultClasses.active : defaultClasses.inactive }><h3>Services</h3></div>
                        {toggle3 && <div>{Services}</div>}
                </div>
                <div className={defaultClasses.footerblock}>
                    <div onClick={() => setToggle4(!toggle4)} className={toggle4 ? defaultClasses.active : defaultClasses.inactive }><h3>Customer care</h3></div>
                        {toggle4 && <div>{customerCare}</div>}
                </div>
            </div>
        </div>
        <div className={defaultClasses.desktopfooterlinkssection}>
            <div className={defaultClasses.footersubsection}>
                <div className={defaultClasses.footerblock}>
                    <h3>Get in touch</h3>
                    <div>{getInTouch}</div>
                </div>
                <div className={defaultClasses.footerblock}>
                    <h3>About us</h3>
                    <div>{aboutUs}</div>
                </div>
                <div className={defaultClasses.footerblock}>
                    <h3>Services</h3>
                    <div>{Services}</div>
                </div>
                <div className={defaultClasses.footerblock}>
                    <h3>Customer care</h3>
                    <div>{customerCare}</div>
                </div>
            </div>
        </div>
        </Fragment>
    );
};
export default links;
