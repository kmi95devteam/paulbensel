import React from 'react';
import { Facebook, Instagram, Twitter } from 'react-feather';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { shape, string } from 'prop-types';
import { useFooter } from '@magento/peregrine/lib/talons/Footer/useFooter';

import Logo from '@magento/venia-ui/lib/components/Logo';
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import defaultClasses from './footer.css';
import CmsBlockGroup from "../CmsBlock";
import Image from '@magento/venia-ui/lib/components/Image';
import Partnerlogo1 from './image/image-1.png';
import Partnerlogo2 from './image/image-2.png';
import Partnerlogo3 from './image/image-3.png';
import Newsletter from '../Newsletter';
import Cmsfooterlinks from '../CmsFooterLinks';

const Footer = props => {
    const { links } = props;
    const classes = mergeClasses(defaultClasses, props.classes);
    const talonProps = useFooter();
    const privacyLinks = CmsBlockGroup({"identifiers": "privacyblock"});
    const { copyrightText } = talonProps;
    return (
        <footer className={classes.root}>
            <div className={classes.footersection}>
            <Cmsfooterlinks />
            <Newsletter />
            </div>
            <div className={classes.footersectionpartner}>
                <div className={classes.footerlogosection}>
                    <Image
                    src={Partnerlogo1}
                    />
                    <Image
                    src={Partnerlogo2}
                    />
                    <Image
                    src={Partnerlogo3}
                    />
                </div>
                <div className={classes.callout}>
                    <ul className={classes.socialLinks}>
                    <li>
                    <Instagram size={20} />
                    </li>
                    <li>
                    <Facebook size={20} />
                    </li>
                    <li>
                    <Twitter size={20} />
                    </li>
                    </ul>
                </div>
            </div>
            <div className={classes.copyrightsection}>
                <div className={classes.copyrights}>
                    <p className={classes.copyright}>{copyrightText || null}</p>
                </div>
                {privacyLinks}
            </div>
        </footer>
    );
};

export default Footer;

Footer.propTypes = {
    classes: shape({
        root: string,
        footerblock:string,
        footersection:string,
        footersectionpartner:string,
        footerlogosection:string,
        footeractions:string,
        logos:string,
        footerlogos:string,
        subscribeButton:string,
        content:string,
        copyrightsection:string,
        copyrightlinks:string,
        copyright:string,
        footerblockemail:string
    })
};
