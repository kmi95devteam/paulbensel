import React from 'react';
import defaultClasses from './topCmsLinks.css';

const TopCmsLinks = () => {
    return (
        <div className={defaultClasses.cmsTopLinks}>
            <ul className={defaultClasses.cmsTop}>
                <li><a href="#">Jewelry</a></li>
                <li><a href="#">Engagement & Bridal</a></li>
                <li><a href="#">Custom Jewelry</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">About Us</a></li>
            </ul>
        </div>
    );
};

export default TopCmsLinks;
