import React, { Fragment } from 'react';
import cmsBlock from "../CmsBlock";
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import benselExperience from './css/benselExperience.css';
import banner from './css/banner.css';
import Dreambig from './css/dreambig.css';
import shopByCategory from './css/shopByCategory.css';
import myBensel from './css/mybensel.css';
import BannerJs from '../Banner';

const HomePage = () => {
    //const classes = mergeClasses(benselExperience, banner);
    const benselExperienceContent = cmsBlock({"identifiers": "benselExperience"});
    const Banner = cmsBlock({"identifiers": "homebanner"});
    const shopByCategory = cmsBlock({"identifiers": "shopByCategory"});
    const Dreambig = cmsBlock({"identifiers": "dreambig"});
    const myBensel = cmsBlock({"identifiers": "mybensel"});
    const MustHave = cmsBlock({"identifiers": "musthave"});
    return (
        <Fragment>
        <BannerJs />
        <div className={benselExperience.homeComponents}>{benselExperienceContent}</div>
        <div className={benselExperience.homeComponents}>{shopByCategory}</div>
        <div className={benselExperience.homeComponents}>{MustHave}</div>
        <div className={benselExperience.homeComponents}>{Dreambig}</div>
        <div className={benselExperience.homeComponents}>{myBensel}</div>
        </Fragment>
    );
};

export default HomePage;
