import React, { Fragment, useEffect } from 'react';
import { Form } from 'informed';
import { FormattedMessage, useIntl } from 'react-intl';
import defaultClasses from './newsletter.css';
import FormError from '@magento/venia-ui/lib/components/FormError/formError';
import { isRequired } from '@magento/venia-ui/lib/util/formValidators';
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import Button from '@magento/venia-ui/lib/components/Button';
import LoadingIndicator from '@magento/venia-ui/lib/components/LoadingIndicator';
import TextInput from '@magento/venia-ui/lib/components/TextInput';
import { useNewsletter } from 'src/peregrine/lib/talons/Newsletter/useNewsletter';
import { SUBSCRIBE_TO_NEWSLETTER } from './newsletter.gql';
import { useToasts } from '@magento/peregrine';
const Newsletter = props => {
    const { formatMessage } = useIntl();
    const classes = mergeClasses(defaultClasses, props.classes);
    const talonProps = useNewsletter({
        subscribeMutation: SUBSCRIBE_TO_NEWSLETTER
    });
    const [, { addToast }] = useToasts();
    const {
        errors,
        handleSubmit,
        isBusy,
        setFormApi,
        newsLetterResponse
    } = talonProps;
    useEffect(() => {
        if (newsLetterResponse && newsLetterResponse.status) {
            addToast({
                type: 'info',
                message: formatMessage({
                    id: 'newsletter.subscribeMessage',
                    defaultMessage: 'The email address is subscribed.'
                }),
                timeout: 5000
            });
        }
    }, [addToast, newsLetterResponse]);
    if (isBusy) {
        return (
            <div className={classes.modal_active}>
            <LoadingIndicator>
            <FormattedMessage
        id={'newsletter.loadingText'}
        defaultMessage={'Subscribing'}
        />
        </LoadingIndicator>
        </div>
    );
    }
    return (
        <Fragment>
        <div className={classes.root}>
    <FormError errors={Array.from(errors.values())} />
    <Form
    getApi={setFormApi}
    className={classes.footerblockemail}
    onSubmit={handleSubmit}
        >
        <h3>Sign up for emails and get 15% OFF on first order</h3>
    <div className={classes.content}>
        <TextInput
    autoComplete="email"
    field="email"
    validate={isRequired}
    placeholder="Enter your email"
    />
    </div>
    <div className={classes.footeractions}>
        <Button priority="high" type="submit" className={classes.subscribeButton}>
        <FormattedMessage
    id={'newsletter.subscribeText'}
    defaultMessage={'Sign Up'}
    />
    </Button>
    </div>
    </Form>
    </div>
    </Fragment>
);
};
export default Newsletter;
