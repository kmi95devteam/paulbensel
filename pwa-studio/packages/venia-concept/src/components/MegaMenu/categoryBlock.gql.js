import gql from 'graphql-tag'

const GET_CATEGORY_QUERY = gql`
query category($id: Int )
{
	categoryId(id: $id)
  {
    id
    cms_block_list
    block_id
    identifier
    content      
  }
}
`;

export default {
    queries: {
        getCategoryQuery: GET_CATEGORY_QUERY
    },
    mutations: {}
};