import React from 'react';
import categoryBlock from './categoryBlock.gql';
import { useQuery } from '@apollo/client';
import cmsBlock from "@magento/venia-ui/lib/components/CmsBlock";
import classes from './menuBlock.css';

const MenuBlock = props => {
    const { categoryID } = props;

    const { queries } = categoryBlock;
    const { getCategoryQuery } = queries;

    const { data, error, loading } = useQuery(getCategoryQuery, {
        variables: { id: categoryID }
    });


    if (loading) return <p>Loading</p>;
    if (error) return <p>Error</p>;

    const menuBlockIdentifier = data.categoryId.map(categoryContent => {
        return cmsBlock({"identifiers": categoryContent.identifier});
    });
    const mainBlock = Object.keys(menuBlockIdentifier).length !== 0 ? classes.menuBlock : "";

    return (
        <div className={mainBlock}>
        {menuBlockIdentifier}
        </div>
);
};

export default MenuBlock;